# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Kheyyam Gojayev <xxmn77@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-01-29 00:45+0000\n"
"PO-Revision-Date: 2021-09-02 14:53+0400\n"
"Last-Translator: Kheyyam Gojayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"

#: contents/ui/Config.qml:35
#, kde-format
msgid "Number of Columns:"
msgstr "Sütunların sayı:"

#: contents/ui/Config.qml:42
#, kde-format
msgctxt "@label"
msgid "Automatic"
msgstr "Öz-özünə"

#: contents/ui/Config.qml:54
#, kde-format
msgid "Display Style:"
msgstr "Görüntü üslubu:"
