# Translation of ksgrd.po to Catalan
# Copyright (C) 2013-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2013, 2017, 2019, 2020, 2021.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015, 2016, 2020.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-01 00:45+0000\n"
"PO-Revision-Date: 2021-10-12 19:37+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#: SensorAgent.cpp:87
#, kde-format
msgctxt "%1 is a host name"
msgid ""
"Message from %1:\n"
"%2"
msgstr ""
"Missatge des de %1:\n"
"%2"

#: SensorManager.cpp:49
#, kde-format
msgid "Change"
msgstr "Canvi"

#: SensorManager.cpp:50
#, kde-format
msgid "Rate"
msgstr "Velocitat"

#: SensorManager.cpp:52
#, kde-format
msgid "CPU Load"
msgstr "Càrrega de la CPU"

#: SensorManager.cpp:53
#, kde-format
msgid "Idling"
msgstr "En repòs"

#: SensorManager.cpp:54
#, kde-format
msgid "Nice Load"
msgstr "Càrrega de prioritat"

#: SensorManager.cpp:55
#, kde-format
msgid "User Load"
msgstr "Càrrega d'usuari"

#: SensorManager.cpp:56
#, kde-format
msgctxt "@item sensor description"
msgid "System Load"
msgstr "Càrrega del sistema"

#: SensorManager.cpp:57
#, kde-format
msgid "Waiting"
msgstr "En espera"

#: SensorManager.cpp:58
#, kde-format
msgid "Interrupt Load"
msgstr "Càrrega d'interrupcions"

#: SensorManager.cpp:59
#, kde-format
msgid "Total Load"
msgstr "Càrrega total"

#: SensorManager.cpp:61
#, kde-format
msgid "Memory"
msgstr "Memòria"

#: SensorManager.cpp:62
#, kde-format
msgid "Physical Memory"
msgstr "Memòria física"

#: SensorManager.cpp:63
#, kde-format
msgid "Total Memory"
msgstr "Memòria total"

#: SensorManager.cpp:64
#, kde-format
msgid "Swap Memory"
msgstr "Memòria d'intercanvi"

#: SensorManager.cpp:65
#, kde-format
msgid "Cached Memory"
msgstr "Memòria cau"

#: SensorManager.cpp:66
#, kde-format
msgid "Buffered Memory"
msgstr "Memòria intermèdia"

#: SensorManager.cpp:67
#, kde-format
msgid "Used Memory"
msgstr "Memòria usada"

#: SensorManager.cpp:68
#, kde-format
msgid "Application Memory"
msgstr "Memòria d'aplicacions"

#: SensorManager.cpp:69
#, kde-format
msgid "Allocated Memory"
msgstr "Memòria assignada"

#: SensorManager.cpp:70
#, kde-format
msgid "Free Memory"
msgstr "Memòria lliure"

#: SensorManager.cpp:71
#, kde-format
msgid "Available Memory"
msgstr "Memòria disponible"

#: SensorManager.cpp:72
#, kde-format
msgid "Active Memory"
msgstr "Memòria activa"

#: SensorManager.cpp:73
#, kde-format
msgid "Inactive Memory"
msgstr "Memòria inactiva"

#: SensorManager.cpp:74
#, kde-format
msgid "Wired Memory"
msgstr "Memòria capturada"

#: SensorManager.cpp:75
#, kde-format
msgid "Exec Pages"
msgstr "Pàgines executables"

#: SensorManager.cpp:76
#, kde-format
msgid "File Pages"
msgstr "Pàgines de fitxers"

#: SensorManager.cpp:79
#, kde-format
msgid "Processes"
msgstr "Processos"

#: SensorManager.cpp:80 SensorManager.cpp:257
#, kde-format
msgid "Process Controller"
msgstr "Controlador de processos"

#: SensorManager.cpp:81
#, kde-format
msgid "Last Process ID"
msgstr "ID del darrer procés"

#: SensorManager.cpp:82
#, kde-format
msgid "Process Spawn Count"
msgstr "Comptador d'activacions de procés"

#: SensorManager.cpp:83
#, kde-format
msgid "Process Count"
msgstr "Comptador de processos"

#: SensorManager.cpp:84
#, kde-format
msgid "Idle Processes Count"
msgstr "Comptador de processos en repòs"

#: SensorManager.cpp:85
#, kde-format
msgid "Running Processes Count"
msgstr "Comptador de processos en execució"

#: SensorManager.cpp:86
#, kde-format
msgid "Sleeping Processes Count"
msgstr "Comptador de processos adormits"

#: SensorManager.cpp:87
#, kde-format
msgid "Stopped Processes Count"
msgstr "Comptador de processos aturats"

#: SensorManager.cpp:88
#, kde-format
msgid "Zombie Processes Count"
msgstr "Comptador de processos zombis"

#: SensorManager.cpp:89
#, kde-format
msgid "Waiting Processes Count"
msgstr "Comptador de processos en espera"

#: SensorManager.cpp:90
#, kde-format
msgid "Locked Processes Count"
msgstr "Comptador de processos bloquejats"

#: SensorManager.cpp:92
#, kde-format
msgid "Disk Throughput"
msgstr "Rendiment del disc"

#: SensorManager.cpp:93
#, kde-format
msgctxt "CPU Load"
msgid "Load"
msgstr "Càrrega"

#: SensorManager.cpp:94
#, kde-format
msgid "Total Accesses"
msgstr "Accessos totals"

#: SensorManager.cpp:95
#, kde-format
msgid "Read Accesses"
msgstr "Accessos de lectura"

#: SensorManager.cpp:96
#, kde-format
msgid "Write Accesses"
msgstr "Accessos d'escriptura"

#: SensorManager.cpp:97
#, kde-format
msgid "Read Data"
msgstr "Dades llegides"

#: SensorManager.cpp:98
#, kde-format
msgid "Written Data"
msgstr "Dades escrites"

#: SensorManager.cpp:99
#, kde-format
msgid "Milliseconds spent reading"
msgstr "Mil·lisegons dedicats en lectura"

#: SensorManager.cpp:100
#, kde-format
msgid "Milliseconds spent writing"
msgstr "Mil·lisegons dedicats en escriptura"

#: SensorManager.cpp:101
#, kde-format
msgid "I/Os currently in progress"
msgstr "E/S actualment en curs"

#: SensorManager.cpp:102
#, kde-format
msgid "Pages In"
msgstr "Pàgines rebudes"

#: SensorManager.cpp:103
#, kde-format
msgid "Pages Out"
msgstr "Pàgines enviades"

#: SensorManager.cpp:104
#, kde-format
msgid "Context Switches"
msgstr "Commutacions de context"

#: SensorManager.cpp:105
#, kde-format
msgid "Traps"
msgstr "Rutines d'interrupció"

#: SensorManager.cpp:106
#, kde-format
msgid "System Calls"
msgstr "Crides del sistema"

#: SensorManager.cpp:107
#, kde-format
msgid "Network"
msgstr "Xarxa"

#: SensorManager.cpp:108
#, kde-format
msgid "Interfaces"
msgstr "Interfícies"

#: SensorManager.cpp:109
#, kde-format
msgid "Receiver"
msgstr "Receptor"

#: SensorManager.cpp:110
#, kde-format
msgid "Transmitter"
msgstr "Transmissor"

#: SensorManager.cpp:112
#, kde-format
msgid "Data Rate"
msgstr "Velocitat de dades"

#: SensorManager.cpp:113
#, kde-format
msgid "Compressed Packets Rate"
msgstr "Velocitat de paquets comprimits"

#: SensorManager.cpp:114
#, kde-format
msgid "Dropped Packets Rate"
msgstr "Velocitat de paquets perduts"

#: SensorManager.cpp:115
#, kde-format
msgid "Error Rate"
msgstr "Taxa d'errors"

#: SensorManager.cpp:116
#, kde-format
msgid "FIFO Overruns Rate"
msgstr "Taxa de desbordament de la FIFO"

#: SensorManager.cpp:117
#, kde-format
msgid "Frame Error Rate"
msgstr "Taxa d'errors de trama"

#: SensorManager.cpp:118
#, kde-format
msgid "Multicast Packet Rate"
msgstr "Taxa de paquets de multidifusió"

#: SensorManager.cpp:119
#, kde-format
msgid "Packet Rate"
msgstr "Taxa de paquets"

#: SensorManager.cpp:120
#, kde-format
msgctxt "@item sensor description ('carrier' is a type of network signal)"
msgid "Carrier Loss Rate"
msgstr "Taxa de pèrdua de portadora"

#: SensorManager.cpp:121 SensorManager.cpp:132
#, kde-format
msgid "Collisions"
msgstr "Col·lisions"

#: SensorManager.cpp:123
#, kde-format
msgid "Data"
msgstr "Dades"

#: SensorManager.cpp:124
#, kde-format
msgid "Compressed Packets"
msgstr "Paquets comprimits"

#: SensorManager.cpp:125
#, kde-format
msgid "Dropped Packets"
msgstr "Paquets perduts"

#: SensorManager.cpp:126
#, kde-format
msgid "Errors"
msgstr "Errors"

#: SensorManager.cpp:127
#, kde-format
msgid "FIFO Overruns"
msgstr "Desbordament de la FIFO"

#: SensorManager.cpp:128
#, kde-format
msgid "Frame Errors"
msgstr "Errors de trama"

#: SensorManager.cpp:129
#, kde-format
msgid "Multicast Packets"
msgstr "Paquets de multidifusió"

#: SensorManager.cpp:130
#, kde-format
msgid "Packets"
msgstr "Paquets"

#: SensorManager.cpp:131
#, kde-format
msgctxt "@item sensor description ('carrier' is a type of network signal)"
msgid "Carrier Losses"
msgstr "Pèrdua de portadora"

#: SensorManager.cpp:135
#, kde-format
msgid "Sockets"
msgstr "Sòcols"

#: SensorManager.cpp:136
#, kde-format
msgid "Total Number"
msgstr "Nombre total"

#: SensorManager.cpp:137 SensorManager.cpp:258
#, kde-format
msgid "Table"
msgstr "Taula"

#: SensorManager.cpp:138
#, kde-format
msgid "Advanced Power Management"
msgstr "Gestió avançada d'energia"

#: SensorManager.cpp:139
#, kde-format
msgid "ACPI"
msgstr "ACPI"

#: SensorManager.cpp:140
#, kde-format
msgid "Cooling Device"
msgstr "Dispositiu de refrigeració"

#: SensorManager.cpp:141
#, kde-format
msgid "Current State"
msgstr "Estat actual"

#: SensorManager.cpp:142 SensorManager.cpp:143
#, kde-format
msgid "Thermal Zone"
msgstr "Zona tèrmica"

#: SensorManager.cpp:144 SensorManager.cpp:145
#, kde-format
msgid "Temperature"
msgstr "Temperatura"

#: SensorManager.cpp:146
#, kde-format
msgid "Average CPU Temperature"
msgstr "Temperatura mitjana de la CPU"

#: SensorManager.cpp:147
#, kde-format
msgid "Fan"
msgstr "Ventilador"

#: SensorManager.cpp:148
#, kde-format
msgid "State"
msgstr "Estat"

#: SensorManager.cpp:149
#, kde-format
msgid "Battery"
msgstr "Bateria"

#: SensorManager.cpp:150
#, kde-format
msgid "Battery Capacity"
msgstr "Capacitat de la bateria"

#: SensorManager.cpp:151
#, kde-format
msgid "Battery Charge"
msgstr "Càrrega de la bateria"

#: SensorManager.cpp:152
#, kde-format
msgid "Battery Usage"
msgstr "Ús de la bateria"

#: SensorManager.cpp:153
#, kde-format
msgid "Battery Voltage"
msgstr "Voltatge de la bateria"

#: SensorManager.cpp:154
#, kde-format
msgid "Battery Discharge Rate"
msgstr "Velocitat de descàrrega de la bateria"

#: SensorManager.cpp:155
#, kde-format
msgid "Remaining Time"
msgstr "Temps restant"

#: SensorManager.cpp:156
#, kde-format
msgid "Interrupts"
msgstr "Interrupcions"

#: SensorManager.cpp:157
#, kde-format
msgid "Load Average (1 min)"
msgstr "Càrrega mitjana (1 min)"

#: SensorManager.cpp:158
#, kde-format
msgid "Load Average (5 min)"
msgstr "Càrrega mitjana (5 min)"

#: SensorManager.cpp:159
#, kde-format
msgid "Load Average (15 min)"
msgstr "Càrrega mitjana (15 min)"

#: SensorManager.cpp:160
#, kde-format
msgid "Clock Frequency"
msgstr "Freqüència del rellotge"

#: SensorManager.cpp:161
#, kde-format
msgid "Average Clock Frequency"
msgstr "Freqüència mitjana del rellotge"

#: SensorManager.cpp:162
#, kde-format
msgid "Hardware Sensors"
msgstr "Sensors de maquinari"

#: SensorManager.cpp:163
#, kde-format
msgid "Partition Usage"
msgstr "Ús de les particions"

#: SensorManager.cpp:164
#, kde-format
msgid "Used Space"
msgstr "Espai usat"

#: SensorManager.cpp:165
#, kde-format
msgid "Free Space"
msgstr "Espai lliure"

#: SensorManager.cpp:166
#, kde-format
msgid "Fill Level"
msgstr "Nivell d'ocupació"

#: SensorManager.cpp:167
#, kde-format
msgid "Used Inodes"
msgstr "Inodes usats"

#: SensorManager.cpp:168
#, kde-format
msgid "Free Inodes"
msgstr "Inodes lliures"

#: SensorManager.cpp:169
#, kde-format
msgid "Inode Level"
msgstr "Nivell d'inodes"

#: SensorManager.cpp:170
#, kde-format
msgid "System"
msgstr "Sistema"

#: SensorManager.cpp:171
#, kde-format
msgid "Uptime"
msgstr "En funcionament"

#: SensorManager.cpp:172
#, kde-format
msgid "Linux Soft Raid (md)"
msgstr "RAID de Linux per programari (md)"

#: SensorManager.cpp:173
#, kde-format
msgid "Processors"
msgstr "Processadors"

#: SensorManager.cpp:174
#, kde-format
msgid "Cores"
msgstr "Nuclis"

#: SensorManager.cpp:175
#, kde-format
msgid "Number of Blocks"
msgstr "Nombre de blocs"

#: SensorManager.cpp:176
#, kde-format
msgid "Total Number of Devices"
msgstr "Nombre total de dispositius"

#: SensorManager.cpp:177
#, kde-format
msgid "Failed Devices"
msgstr "Dispositius amb fallades"

#: SensorManager.cpp:178
#, kde-format
msgid "Spare Devices"
msgstr "Dispositius disponibles"

#: SensorManager.cpp:179
#, kde-format
msgid "Number of Raid Devices"
msgstr "Nombre de dispositius RAID"

#: SensorManager.cpp:180
#, kde-format
msgid "Working Devices"
msgstr "Dispositius en funcionament"

#: SensorManager.cpp:181
#, kde-format
msgid "Active Devices"
msgstr "Dispositius actius"

#: SensorManager.cpp:182
#, kde-format
msgid "Number of Devices"
msgstr "Nombre de dispositius"

#: SensorManager.cpp:183
#, kde-format
msgid "Resyncing Percent"
msgstr "Percentatge de resincronització"

#: SensorManager.cpp:184
#, kde-format
msgid "Disk Information"
msgstr "Informació del disc"

#: SensorManager.cpp:185
#, kde-format
msgid "CPU Temperature"
msgstr "Temperatura de la CPU"

#: SensorManager.cpp:186
#, kde-format
msgid "Motherboard Temperature"
msgstr "Temperatura de la placa base"

#: SensorManager.cpp:187
#, kde-format
msgid "Power Supply Temperature"
msgstr "Temperatura de la font d'alimentació"

#: SensorManager.cpp:189
#, kde-format
msgid "Filesystem Root"
msgstr "Arrel del sistema de fitxers"

#: SensorManager.cpp:192
#, kde-format
msgid "Extra Temperature Sensor %1"
msgstr "Sensor addicional de temperatura %1"

#: SensorManager.cpp:196
#, kde-format
msgid "PECI Temperature Sensor %1"
msgstr "Sensor PECI de temperatura %1"

#: SensorManager.cpp:197
#, kde-format
msgid "PECI Temperature Calibration %1"
msgstr "Calibratge PECI de la temperatura %1"

#: SensorManager.cpp:201
#, kde-format
msgid "CPU %1"
msgstr "CPU %1"

#: SensorManager.cpp:202
#, kde-format
msgid "Disk %1"
msgstr "Disc %1"

#: SensorManager.cpp:206
#, kde-format
msgid "Battery %1"
msgstr "Bateria %1"

#: SensorManager.cpp:207
#, kde-format
msgid "Fan %1"
msgstr "Ventilador %1"

#: SensorManager.cpp:208
#, kde-format
msgid "Temperature %1"
msgstr "Temperatura %1"

#: SensorManager.cpp:211
#, kde-format
msgid "Total"
msgstr "Total"

#: SensorManager.cpp:212
#, kde-format
msgid "Software Interrupts"
msgstr "Interrupcions de programari"

#: SensorManager.cpp:213
#, kde-format
msgid "Hardware Interrupts"
msgstr "Interrupcions de maquinari"

#: SensorManager.cpp:218 SensorManager.cpp:220
#, kde-format
msgid "Int %1"
msgstr "Int %1"

#: SensorManager.cpp:223
#, kde-format
msgid "Link Quality"
msgstr "Qualitat de l'enllaç"

#: SensorManager.cpp:224
#, kde-format
msgid "Signal Level"
msgstr "Intensitat del senyal"

#: SensorManager.cpp:225
#, kde-format
msgid "Noise Level"
msgstr "Nivell de soroll"

#: SensorManager.cpp:226
#, kde-format
msgid "Rx Invalid Nwid Packets"
msgstr "Paquets Rx amb la Nwid no vàlida"

#: SensorManager.cpp:227
#, kde-format
msgid "Total Rx Invalid Nwid Packets"
msgstr "Total de paquets Rx amb la Nwid no vàlida"

#: SensorManager.cpp:228
#, kde-format
msgid "Rx Invalid Crypt Packets"
msgstr "Paquets Rx amb l'encriptació no vàlida"

#: SensorManager.cpp:229
#, kde-format
msgid "Total Rx Invalid Crypt Packets"
msgstr "Total de paquets Rx amb l'encriptació no vàlida"

#: SensorManager.cpp:230
#, kde-format
msgid "Rx Invalid Frag Packets"
msgstr "Paquets Rx amb la fragmentació no vàlida"

#: SensorManager.cpp:231
#, kde-format
msgid "Total Rx Invalid Frag Packets"
msgstr "Total de paquets Rx amb la fragmentació no vàlida"

#: SensorManager.cpp:232
#, kde-format
msgid "Tx Excessive Retries Packets"
msgstr "Paquets de reintent amb Tx excessiva"

#: SensorManager.cpp:233
#, kde-format
msgid "Total Tx Excessive Retries Packets"
msgstr "Total de paquets de reintent amb Tx excessiva"

#: SensorManager.cpp:234
#, kde-format
msgid "Invalid Misc Packets"
msgstr "Paquets diversos no vàlids"

#: SensorManager.cpp:235
#, kde-format
msgid "Total Invalid Misc Packets"
msgstr "Total de paquets diversos no vàlids"

#: SensorManager.cpp:236
#, kde-format
msgid "Missed Beacons"
msgstr "Senyals perduts"

#: SensorManager.cpp:237
#, kde-format
msgid "Total Missed Beacons"
msgstr "Total de senyals perduts"

#: SensorManager.cpp:239
#, kde-format
msgid "Log Files"
msgstr "Fitxers de registre"

#: SensorManager.cpp:243
#, kde-format
msgctxt "the unit 1 per second"
msgid "1/s"
msgstr "1/s"

#: SensorManager.cpp:244
#, kde-format
msgid "kBytes"
msgstr "kBytes"

#: SensorManager.cpp:245
#, kde-format
msgctxt "the unit minutes"
msgid "min"
msgstr "min"

#: SensorManager.cpp:246
#, kde-format
msgctxt "the frequency unit"
msgid "MHz"
msgstr "MHz"

#: SensorManager.cpp:247
#, kde-format
msgctxt "a percentage"
msgid "%"
msgstr "%"

#: SensorManager.cpp:248
#, kde-format
msgctxt "the unit milliamperes"
msgid "mA"
msgstr "mA"

#: SensorManager.cpp:249
#, kde-format
msgctxt "the unit milliampere hours"
msgid "mAh"
msgstr "mAh"

#: SensorManager.cpp:250
#, kde-format
msgctxt "the unit milliwatts"
msgid "mW"
msgstr "mW"

#: SensorManager.cpp:251
#, kde-format
msgctxt "the unit milliwatt hours"
msgid "mWh"
msgstr "mWh"

#: SensorManager.cpp:252
#, kde-format
msgctxt "the unit millivolts"
msgid "mV"
msgstr "mV"

#: SensorManager.cpp:255
#, kde-format
msgid "Integer Value"
msgstr "Valor enter"

#: SensorManager.cpp:256
#, kde-format
msgid "Floating Point Value"
msgstr "Valor de coma flotant"

#: SensorManager.cpp:259
#, kde-format
msgid "Log File"
msgstr "Fitxer de registre"

#: SensorShellAgent.cpp:108
#, kde-format
msgid "Could not run daemon program '%1'."
msgstr "No s'ha pogut executar el programa del dimoni «%1»."

#: SensorShellAgent.cpp:115
#, kde-format
msgid "The daemon program '%1' failed."
msgstr "El programa del dimoni «%1» ha fallat."

#: SensorSocketAgent.cpp:89
#, kde-format
msgid "Connection to %1 refused"
msgstr "S'ha rebutjat la connexió a %1"

#: SensorSocketAgent.cpp:92
#, kde-format
msgid "Host %1 not found"
msgstr "No s'ha trobat la màquina %1"

#: SensorSocketAgent.cpp:95
#, kde-format
msgid ""
"An error occurred with the network (e.g. the network cable was accidentally "
"unplugged) for host %1."
msgstr ""
"Hi ha hagut un error de xarxa amb la màquina %1 (p. ex. el cable de xarxa "
"s'ha desconnectat accidentalment)."

#: SensorSocketAgent.cpp:98
#, kde-format
msgid "Error for host %1: %2"
msgstr "Error per a la màquina %1: %2"
